from root.common.hparams import params
# from root.common.utils import reshape_and_pad
from tensorflow.python.ops import array_ops, control_flow_ops, gen_array_ops, gen_math_ops, math_ops, logging_ops
from tensorflow.nn import raw_rnn
from root.model.cell import seq_cell
from tensorflow.python.framework.dtypes import int32 as tf_int32
import tensorflow as tf


class trainer:

    """
        It works as an alternative to dynamic_rnn because we can not
        give input to wavernn_cell using dynamic_rnn.

        This encoder only supports wavernn module.

    """

    def __init__(self):

        self._cell = seq_cell()

    def __call__(self, inputs):

        with tf.name_scope("feat_restructure"):

            inp_shape = array_ops.shape(inputs)[0]

            self.batch_size = math_ops.cast(math_ops.floordiv(
                inp_shape, params.seq_length)+1, tf.int32)

            self.pad_length = params.seq_length * self.batch_size - inp_shape

            padded_inp = array_ops.pad(inputs,  [[0, self.pad_length], [0, 0]])
            padded = tf.reshape(padded_inp, [-1, params.seq_length, 4])
            transposed = tf.transpose(padded, [1, 0, 2])

        with tf.name_scope("coarse"):
            mask = [True, True, False, True]
            coarse_inp = tf.boolean_mask(transposed, mask, axis=2)
            coarse_inp.set_shape([params.seq_length, None, 3])

        with tf.name_scope("fine"):
            mask = [True, True, True, False]
            fine_inp = tf.boolean_mask(transposed, mask, axis=2)
            fine_inp.set_shape([params.seq_length, None, 3])

        with tf.name_scope("compute"):
            inputs = self._cell.container(coarse=coarse_inp, fine=fine_inp)

        return self._cell(inputs, self.pad_length)
