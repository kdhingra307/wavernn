import tensorflow as tf
from root.common.feeder import input_feeder
from root.common.hparams import params
from root.model.wavernn_cell import wavernn_cell
from root.model.wave_encoder import wavernn_encoder


class wavernn:

    def __inputs__(self):

        self.data = input_feeder.iterator

        self.mel_data, self.linear_input, self.linear_ground_truth = self.data.get_next()

    def __prenet__(self):

        with tf.name_scope("prenet"):

            embedding = tf.layers.dense(inputs=self.mel_data,
                                        units=params.hop_size,
                                        name="embed")

            return tf.layers.dropout(inputs=embedding,
                                     units=params.prenet_dropout)

    def __seqnet__(self):

        with tf.name_scope("prenet"):

            seq_cell = wavernn_cell()
            mel_embedding = self.__prenet__()
            encoder = wavernn_encoder(mel_embedding,
                                      self.linear_input,
                                      self.linear_ground_truth)
            output, final_state = encoder()

            print(output)

    def __init__(self):

        self.__inputs__()
        self.__seqnet__()
