import tensorflow as tf
from collections import namedtuple
from tensorflow.python.layers import core
from tensorflow.python.ops import array_ops, math_ops, nn_ops
from tensorflow.python.framework.ops import name_scope
from tensorflow.python.ops.variable_scope import variable_scope
from root.common.hparams import params


# if params.use_gpu:
from tensorflow.contrib.cudnn_rnn import CudnnGRU as gru_cell
# else:
"""

        For CPU only:

        if you want to retrain this network from starting then
            replace 
                from tensorflow.contrib.cudnn_rnn import CudnnCompatibleGRUCell as gru_cell
            with 
                from tensorflow.contrib.rnn import GRUBlockCellV2

        Note: GRUBlockCellV2 is faster but not compatible with parameters CudnnGRU.

    """

# from tensorflow.contrib.cudnn_rnn import CudnnCompatibleGRUCell as gru_cell


class seq_cell:

    def __initialize_cells(self):

        with variable_scope("wavernn", reuse=tf.AUTO_REUSE):
            self.coarse_cell = gru_cell(
                num_layers=params.seq_layers, num_units=params.seq_units)

        with variable_scope("wavernn", reuse=tf.AUTO_REUSE):
            self.fine_cell = gru_cell(
                num_layers=params.seq_layers, num_units=params.seq_units)

    def __init__(self):

        self.container = namedtuple('wavernn', ['coarse', 'fine'])
        self.__initialize_cells()

    def __call__(self, inputs, pad_length, scope="wavernn_cell"):

        with tf.name_scope("coarse"):
            coarse_output = tf.reshape(self.coarse_cell(
                inputs.coarse)[0], [-1, 256])[-pad_length:]

        with tf.name_scope("fine"):
            fine_output = tf.reshape(self.fine_cell(inputs.coarse)[
                                     0], [-1, 256])[-pad_length:]

        return self.container(coarse=coarse_output, fine=fine_output)
