from root.common.hparams import params


class wavernn_encoder:

        """
            It works as an alternative to dynamic_rnn because we can not
            give input to wavernn_cell using dynamic_rnn.

            This encoder only supports wavernn module.

        """

    def __init__(self, embedded_mel, coarse_frames=None, fine_frames=None):

        flattended_mels = tf.reshape(embedded_mel, shape=[-1])
        pad_length = tf.mod(tf.shape(flattended_mels)[0], params.seq_length)

        self.batch_embedded_mel = self.reshape_and_pad(
            flattended_mels, pad_length)

        self.is_train = False
        if coarse_frames is not None:
            self.coarse_frames = self.reshape_and_pad(
                coarse_frames, pad_length)
            self.fine_frames = self.reshape_and_pad(fine_frames, pad_length)
            self.is_train = True
        self.rnn_cell = sequential_cell(training=self.is_train)


    def reshape_and_pad(self, arr, pad_length):
        padded_seq = tf.pad(arr, [[0, pad_length]])
        return tf.reshape(padded_seq, [-1, params.seq_length])

    def sequencer(self, time, cell_output, cell_state, loop_state):

        if cell_output is None:
            next_cell_state = self.rnn_cell.zero_state(
                self._batch_size, tf.float32)
        else:
            next_cell_state = cell_state

        elements_finished = (time >= params.seq_length)


        if self.is_train:
            next_input = tf.cond(
                elements_finished,
                lambda: (tf.zeros([1,params.seq_length]),tf.zeros([1,params.seq_length]),tf.zeros([1,params.seq_length])),
                lambda: (self.batch_embedded_mel[time], self.coarse_frames[time], self.fine_frames[time]))
        else:
            next_input = tf.cond(
                elements_finished,
                lambda: (tf.zeros([1,params.seq_length]))
                lambda: (self.batch_embedded_mel[time]))

        return (elements_finished, next_input, next_cell_state,
                cell_output, loop_state)

    def __call__(self):        

        return tf.nn.raw_rnn(cell=self.rnn_cell, loop_fn=self.sequencer)
