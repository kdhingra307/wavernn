from root.common.hparams import params
from root.common.utils import reshape_and_pad
from tensorflow.python.ops import array_ops, control_flow_ops, gen_array_ops, gen_math_ops, math_ops, logging_ops
from tensorflow.nn import raw_rnn
from root.model.wavernn_cell import wavernn_cell
from tensorflow.python.framework.dtypes import int32 as tf_int32
import tensorflow as tf


class wavernn_encoder:

    """
        It works as an alternative to dynamic_rnn because we can not
        give input to wavernn_cell using dynamic_rnn.

        This encoder only supports wavernn module.

    """

    def __init__(self, embedded_mel, coarse_frames=None, fine_frames=None):

        # with tf.device('/cpu:0'):

        # flattended_mels = gen_array_ops.reshape(embedded_mel, shape=[-1])

        coarse_Frame_shape = array_ops.shape(coarse_frames)[0]
        self.pad_length = coarse_Frame_shape-gen_math_ops.mod(
            coarse_Frame_shape,
            params.seq_length)

        # self.batch_embedded_mel = array_ops.pad(
        #     flattended_mels, [[0, self.pad_length]])

        self.is_train = False
        if coarse_frames is not None:
            self.coarse_frames = coarse_frames
            self.fine_frames = fine_frames
            self.is_train = True
            self._batch_size = 1
        # self.rnn_cell = wavernn_cell(training=self.is_train)

    def sequencer(self, time, cell_output, cell_state, loop_state):
        if cell_output is None:
            next_cell_state = self.rnn_cell.zero_state(
                self._batch_size, self.batch_embedded_mel.dtype)
        else:
            next_cell_state = cell_state

        elements_finished = (time >= params.seq_length)
        batch_index = self._batch_size * time
        if self.is_train:
            next_input = control_flow_ops.cond(
                elements_finished,
                lambda: (array_ops.zeros([self._batch_size]),
                         array_ops.zeros([self._batch_size]),
                         array_ops.zeros([self._batch_size])),
                lambda: (self.batch_embedded_mel[batch_index:batch_index+self._batch_size],
                         self.coarse_frames[batch_index:batch_index +
                                            self._batch_size],
                         self.fine_frames[batch_index:batch_index+self._batch_size]))
        else:
            next_input = control_flow_ops.cond(
                elements_finished,
                lambda: (array_ops.zeros([self._batch_size]),),
                lambda: (self.batch_embedded_mel[time:time+self._batch_size]),)

        return (elements_finished, next_input, next_cell_state,
                cell_output, loop_state)

    def __call__(self):

        c_1 = tf.pad(self.coarse_frames[:-1], [[1, 0]])
        f_1 = tf.pad(self.fine_frames[:-1], [[1, 0]])

        coarse_input = tf.stack([c_1, f_1], axis=-1)

        fine_input = tf.stack([c_1, f_1, self.coarse_frames], axis=-1)

        coarse_reshaped_inp = tf.expand_dims(
            coarse_input, axis=0)
        fine_reshaped_inp = tf.expand_dims(
            fine_input, axis=0)

        with tf.variable_scope("coarse"):
            coarse_cell = tf.contrib.cudnn_rnn.CudnnGRU(
                num_units=128, num_layers=2)
            coarse_output, _ = coarse_cell(coarse_reshaped_inp)
            coarse_output = tf.layers.dense(coarse_output, units=256)

        with tf.variable_scope("fine"):
            fine_cell = tf.contrib.cudnn_rnn.CudnnGRU(
                num_units=128, num_layers=2)
            fine_output, _ = fine_cell(fine_reshaped_inp)
            fine_output = tf.layers.dense(fine_output, units=256)

        # output, state, _ = raw_rnn(
        #     cell=self.rnn_cell, loop_fn=self.sequencer, scope="wavernn_encoder")

        # coarse = output.coarse.concat(name="output")[:-self.pad_length]
        # fine = output.fine.concat(name="output")[:-self.pad_length]

        # coarse = tf.reshape(coarse_output, [-1, 256])
        # fine = tf.reshape(fine_output, [-1, 256])

        output = (coarse_output, fine_output)
        return (output, self.pad_length)
