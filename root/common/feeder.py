import os
from glob import glob
import tensorflow as tf
import numpy as np
from root.common.hparams import params


class input_feeder:

    def __init__(self):
        self.__input__()

    def __import_data__(self, files):

        inpt = list()
        lbls = list()
        max_len = 0

        # super_control = 2205
        for file in files:
            matrix = np.load(file.decode("utf-8"))
            inpt.append(matrix['input'][:, 22050:22050+1240])
            lbls.append(matrix['ground_truth'][:, 22050:22050+1240])

            # if max_len < len(inpt[-1][0]):
        #         max_len = len(inpt[-1][0])

        seq_length = list()
        # padded_inpt = list()
        # padded_lbls = list()

        # for entry in range(len(inpt)):
        #     seq_length.append(max_len-len(inpt[entry][0]))
        #     padded_inpt.append(
        #         np.pad(inpt[entry], ((0, 0), (0, seq_length[-1])), 'constant'))
        #     padded_lbls.append(
        #         np.pad(lbls[entry], ((0, 0), (0, seq_length[-1])), 'constant'))

        padded_inpt = np.asarray(inpt)
        padded_lbls = np.asarray(lbls)
        seq_length = np.asarray(seq_length)

        # input_ = data[:, 0, :, :]
        # ground_truth = np.reshape(
        #     data[:, 1:, :, :], [-1, max_len, pm.num_mels])

        return padded_inpt, padded_lbls.astype(np.int32), seq_length.astype(np.int32)

        # for file in files:
        #     single_audio_sample = np.load(file.decode("utf-8"))
        #     mel = single_audio_sample['mel']
        #     inputs = single_audio_sample['input']
        #     ground_truth = single_audio_sample['ground_truth']
        # return (mel.astype(np.float32),
        #         inputs.astype(np.float32),
        #         ground_truth.astype(np.int32))

    def __import_batchless__(self, file):

        single_audio_sample = np.load(file.decode("utf-8"))
        mel = single_audio_sample['mel']
        inputs = single_audio_sample['input']
        ground_truth = single_audio_sample['ground_truth']
        return (mel.astype(np.float32),
                inputs.astype(np.float32),
                ground_truth.astype(np.int32))

    def __input__(self):

        # with tf.device('/cpu:0'):
        try:
            training_files = glob("{}/*.npz".format(params.train_data_dir))
            dataset = tf.data.Dataset.from_tensor_slices(
                list(training_files))
            # np_dataset = dataset.shuffle(100)
            # batch_db = dataset.batch(30)
            np_dataset = dataset.map(lambda file_name: tf.py_func(
                func=self.__import_batchless__, inp=[file_name],
                Tout=[tf.float32,  tf.float32, tf.int32]))
            self.iterator = np_dataset.make_initializable_iterator()
        except Exception as e:
            print("Meta data not found, check hparams or raise issue on git")
            exit()
