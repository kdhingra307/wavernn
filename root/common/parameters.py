class params:
    epochs = 10
    sample_rate = 22050
    nFft = 1024
    hop_size = 256
    num_mels = 64
    checkpoint_every = 100
    input_data_dirs = ["/home/pravesh/speech_dataset/speakers/3", ]
    train_data_dir = "training_data"
    log_dir = "log/"

    fmin = 125
    fmax = 7800
    max_db = 100

    num_of_workers = 8
    bit_rate_in_power = 16
    scale_factor = int(hop_size/num_mels)

    seq_units = 128
    seq_layers = 2
    rnn_resolution = 256
    seq_length = 1024
    prenet_dropout = 0.2
    max_input_sequence = 5

    """
        Not working on support for cpu, optimizing the complete model first.
    """

    use_gpu = True
